from datetime import datetime
from django.db import models
from mainapp.settings import MEDIA_DIR_RELATIVE

class Theme(models.Model):
    name = models.CharField(verbose_name='Название', max_length=128)
    lang = models.CharField(max_length=2, verbose_name='Язык')
    path_to_img = models.ImageField(null=True, blank=True, verbose_name='Путь до изображения', max_length=256,
                                    upload_to=MEDIA_DIR_RELATIVE)
    score = models.IntegerField(default=0, verbose_name='Счёт')
    total_answers = models.IntegerField(default=0, verbose_name='Количество ответов')

    class Meta:
        db_table = 'theme'
        verbose_name = 'Тема'
        verbose_name_plural = 'Темы'

    def __str__(self):
        return self.name

    def num_questions(self):
        return self.question_set.count()


class Gamer(models.Model):
    name = models.CharField(max_length=256, unique=True)
    email = models.CharField(max_length=64, unique=True)
    device_token = models.CharField(max_length=64, null=True)
    passw = models.CharField(max_length=64)
    session = models.CharField(max_length=32, null=True, unique=True)
    os = models.CharField(max_length=16, null=True)

    class Meta:
        db_table = 'gamer'

    def __str__(self):
        return f"{self.name} ({self.email})"


class Question(models.Model):
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE, verbose_name='Тема')
    question = models.TextField(null=True, blank=True, verbose_name='Вопрос', max_length=128)
    question_img = models.ImageField(upload_to=MEDIA_DIR_RELATIVE, null=True, blank=True,
                                    verbose_name='Путь до изображения', max_length=256)
    answer_1 = models.CharField(null=True, blank=True, verbose_name='Ответ 1', max_length=128)
    answer_1_img = models.ImageField(upload_to=MEDIA_DIR_RELATIVE, null=True, blank=True,
                                    verbose_name='Путь до изображения', max_length=256)
    answer_2 = models.CharField(null=True, blank=True, verbose_name='Ответ 2', max_length=128)
    answer_2_img = models.ImageField(upload_to=MEDIA_DIR_RELATIVE, null=True, blank=True,
                                    verbose_name='Путь до изображения', max_length=256)
    answer_3 = models.CharField(null=True, blank=True, verbose_name='Ответ 3', max_length=128)
    answer_3_img = models.ImageField(upload_to=MEDIA_DIR_RELATIVE, null=True, blank=True,
                                    verbose_name='Путь до изображения', max_length=256)
    answer_4 = models.CharField(null=True, blank=True, verbose_name='Ответ 4', max_length=128)
    answer_4_img = models.ImageField(upload_to=MEDIA_DIR_RELATIVE, null=True, blank=True,
                                    verbose_name='Путь до изображения', max_length=256)
    correct = models.CharField(max_length=1, verbose_name='Правильный ответ', choices=(
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4')
    ))
    lang = models.CharField(max_length=2, verbose_name='Язык')
    complexity = models.SmallIntegerField(default=1, verbose_name='Сложность')
    timer = models.IntegerField(verbose_name='Таймер', default=15)
    active = models.BooleanField(default=True, verbose_name='Активен')
    author = models.ForeignKey(Gamer, null=True, on_delete=models.PROTECT, verbose_name='Автор')

    class Meta:
        db_table = 'question'
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        constraints = [
            models.CheckConstraint(
                name='question_check_nulls',
                check=(
                    (
                        models.Q(question__isnull=False) |
                        models.Q(question_img__isnull=False)
                    ) &
                    (
                        models.Q(answer_1__isnull=False) |
                        models.Q(answer_1_img__isnull=False)
                    ) &
                    (
                        models.Q(answer_2__isnull=False) |
                        models.Q(answer_2_img__isnull=False)
                    ) &
                    (
                        models.Q(answer_3__isnull=False) |
                        models.Q(answer_3_img__isnull=False)
                    ) &
                    (
                        models.Q(answer_4__isnull=False) |
                        models.Q(answer_4_img__isnull=False)
                    )
                )
            )
        ]

    def __str__(self):
        return self.question


class Game(models.Model):
    start = models.DateTimeField(default=datetime)
    end = models.DateTimeField(null=True)
    moving_user = models.ForeignKey(Gamer, on_delete=models.PROTECT, null=True)
    timer = models.IntegerField(null=True)
    theme = models.ForeignKey(Theme, null=True, on_delete=models.PROTECT)
    overrecord_game = models.ForeignKey('self', null=True, on_delete=models.PROTECT)
    status = models.CharField(max_length=1, null=True, choices=(
        ('w', 'Ожидание соперника'),
        ('g', 'Играется'),
        ('s', 'Выбор темы'),
        ('e', 'Завершена')
    ))

    class Meta:
        db_table = 'game'


class Winners(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    gamer = models.ForeignKey(Gamer, on_delete=models.PROTECT)

    class Meta:
        db_table = 'winners'


class RecordsMan(models.Model):
    theme = models.OneToOneField(Theme, on_delete=models.PROTECT, null=True)
    gamer = models.ForeignKey(Gamer, on_delete=models.PROTECT)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    score = models.IntegerField()

    class Meta:
        db_table = 'recordsman'


class ThemeRating(models.Model):
    theme = models.ForeignKey(Theme, on_delete=models.PROTECT, null=True)
    gamer = models.ForeignKey(Gamer, on_delete=models.PROTECT)
    score = models.IntegerField(default=0)
    total_answers = models.IntegerField(default=0)
    record = models.IntegerField(default=0)
    record_game = models.ForeignKey(Game, null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'themerating'


class GamersQueue(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    gamer = models.ForeignKey(Gamer, on_delete=models.PROTECT)
    order = models.IntegerField()

    class Meta:
        db_table = 'gamersqueue'


class Round(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    round_number = models.SmallIntegerField()
    question = models.ForeignKey(Question, on_delete=models.PROTECT)

    class Meta:
        db_table = 'round'


class RoundMove(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    gamer = models.ForeignKey(Gamer, on_delete=models.PROTECT)
    answer = models.SmallIntegerField(null=True)
    correct = models.BooleanField(null=True)
    timer_left = models.IntegerField(null=True)
    score = models.IntegerField(default=0)

    class Meta:
        db_table = 'roundmove'
