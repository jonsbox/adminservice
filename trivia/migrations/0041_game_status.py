# Generated by Django 3.0.9 on 2020-09-02 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0040_delete_gamethemes'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='status',
            field=models.CharField(choices=[('w', 'Ожидание соперника'), ('g', 'Играется'), ('e', 'Завершена')], max_length=1, null=True),
        ),
    ]
