# Generated by Django 3.0.9 on 2020-08-04 08:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Название')),
                ('lang', models.CharField(max_length=2, verbose_name='Язык')),
                ('path_to_img', models.CharField(max_length=256, null=True, verbose_name='Путь до изображения')),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=128, verbose_name='Вопрос')),
                ('answer_1', models.CharField(max_length=128, verbose_name='Ответ 1')),
                ('answer_2', models.CharField(max_length=128, verbose_name='Ответ 2')),
                ('answer_3', models.CharField(max_length=128, verbose_name='Ответ 3')),
                ('answer_4', models.CharField(max_length=128, verbose_name='Ответ 4')),
                ('correct', models.CharField(choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4')], max_length=1)),
                ('lang', models.CharField(max_length=2, verbose_name='Язык')),
                ('path_to_img', models.CharField(max_length=256, null=True, verbose_name='Путь до изображения')),
                ('theme', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trivia.Theme', verbose_name='Тема')),
            ],
        ),
    ]
