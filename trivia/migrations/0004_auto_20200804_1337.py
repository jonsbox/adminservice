# Generated by Django 3.0.9 on 2020-08-04 10:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0003_question_complexity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='timer',
            field=models.IntegerField(default=15, verbose_name='Таймер'),
        ),
    ]
