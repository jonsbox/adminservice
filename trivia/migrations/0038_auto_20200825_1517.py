# Generated by Django 3.0.9 on 2020-08-25 12:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0037_game_overrecord_game'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recordsman',
            name='theme',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.PROTECT, to='trivia.Theme'),
        ),
    ]
