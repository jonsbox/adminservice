# Generated by Django 3.0.9 on 2020-08-12 13:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0014_auto_20200812_1450'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='path_to_img',
            new_name='answer_1_img',
        ),
        migrations.RemoveField(
            model_name='round',
            name='answer',
        ),
        migrations.RemoveField(
            model_name='round',
            name='correct',
        ),
        migrations.RemoveField(
            model_name='round',
            name='gamer',
        ),
        migrations.AddField(
            model_name='question',
            name='answer_2_img',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Путь до изображения'),
        ),
        migrations.AddField(
            model_name='question',
            name='answer_3_img',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Путь до изображения'),
        ),
        migrations.AddField(
            model_name='question',
            name='answer_4_img',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Путь до изображения'),
        ),
        migrations.AddField(
            model_name='question',
            name='question_img',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Путь до изображения'),
        ),
        migrations.AlterField(
            model_name='question',
            name='answer_1',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Ответ 1'),
        ),
        migrations.AlterField(
            model_name='question',
            name='answer_2',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Ответ 2'),
        ),
        migrations.AlterField(
            model_name='question',
            name='answer_3',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Ответ 3'),
        ),
        migrations.AlterField(
            model_name='question',
            name='answer_4',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Ответ 4'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Вопрос'),
        ),
        migrations.CreateModel(
            name='RoundMove',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.SmallIntegerField(null=True)),
                ('correct', models.BooleanField(null=True)),
                ('gamer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='trivia.Gamer')),
                ('round', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trivia.Round')),
            ],
            options={
                'db_table': 'roundmove',
            },
        ),
    ]
