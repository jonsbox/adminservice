# Generated by Django 3.0.9 on 2020-08-06 13:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0010_auto_20200806_1300'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='question',
            table='question',
        ),
    ]
