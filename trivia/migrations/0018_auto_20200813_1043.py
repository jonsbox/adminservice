# Generated by Django 3.0.9 on 2020-08-13 07:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0017_auto_20200813_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamethemes',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='trivia.Game'),
        ),
    ]
