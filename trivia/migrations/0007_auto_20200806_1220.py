# Generated by Django 3.0.9 on 2020-08-06 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trivia', '0006_auto_20200806_1121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamer',
            name='email',
            field=models.CharField(max_length=64, unique=True),
        ),
        migrations.AlterField(
            model_name='gamer',
            name='name',
            field=models.CharField(max_length=256, unique=True),
        ),
    ]
