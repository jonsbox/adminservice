from django.contrib import admin
from django.utils.html import format_html
from .models import Theme, Question

admin.site.site_header = 'Админка trivia'


class AdminTemplate(admin.AdminSite):
    site_header = 'Админка trivia'


def _get_html(url):
    return format_html('<img src="/{}" width="200px" />'.format(url)) if url else '-'


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    def image_tag_q(self, obj):
        return _get_html(obj.question_img)

    def image_tag_1(self, obj):
        return _get_html(obj.answer_1_img)

    def image_tag_2(self, obj):
        return _get_html(obj.answer_2_img)

    def image_tag_3(self, obj):
        return _get_html(obj.answer_3_img)

    def image_tag_4(self, obj):
        return _get_html(obj.answer_4_img)

    list_display = ('theme', 'question')
    list_select_related = False
    ordering = ('theme', 'question')

    image_tag_q.short_description = 'Картинка'
    image_tag_q.allow_tags = True
    image_tag_1.short_description = 'Картинка'
    image_tag_1.allow_tags = True
    image_tag_2.short_description = 'Картинка'
    image_tag_2.allow_tags = True
    image_tag_3.short_description = 'Картинка'
    image_tag_3.allow_tags = True
    image_tag_4.short_description = 'Картинка'
    image_tag_4.allow_tags = True

    fields = ('theme', 'question', 'question_img', 'image_tag_q',
              'answer_1', 'answer_1_img', 'image_tag_1',
              'answer_2', 'answer_2_img', 'image_tag_2',
              'answer_3', 'answer_3_img', 'image_tag_3',
              'answer_4', 'answer_4_img', 'image_tag_4',
              'correct', 'lang', 'complexity', 'timer', 'active', 'author')
    readonly_fields = ('image_tag_q', 'image_tag_1', 'image_tag_2', 'image_tag_3', 'image_tag_4', 'author')
    search_fields = ['question']


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    def image_tag_html(self, obj):
        return _get_html(obj.path_to_img)

    image_tag_html.short_description = 'Картинка'
    image_tag_html.allow_tags = True
    list_display = ('name', 'num_questions')
    fields = ('name', 'lang', 'path_to_img', 'image_tag_html')
    readonly_fields = ('image_tag_html', 'score', 'total_answers')
