import json
from random import randint
from django.http import HttpResponse
from django.forms.models import model_to_dict
from trivia.models import Question

def random_query(request):
    q = Question.objects.all()
    r = q[randint(0, len(q) - 1)]
    theme_name = r.theme.name
    r = model_to_dict(r)
    r['theme_name'] = theme_name
    return HttpResponse(json.dumps(dict(r)))
